package com.example.onceratsaved

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel:ViewModel() {


    init {
        Log.i("MainViewModel","View Model Creat ...")
    }
     var _score= MutableLiveData<Int>()
    val score:LiveData<Int>
    get() {
        return _score
    }
    override fun onCleared() {
        super.onCleared()
        Log.i("mainviewmodel","View Model Creat ...")
    }
    fun add(num:Int){
        _score.value=num
        _score.value=_score.value?.plus(1)
    }
   //// fun minus(num:Int){
       /// _score.value=num
       /// _score.value=_score.value?.plus(1)
    ///}
}