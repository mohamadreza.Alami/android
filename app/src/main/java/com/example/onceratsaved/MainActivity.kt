package com.example.onceratsaved

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.util.List.of


class MainActivity : AppCompatActivity() {
    private val tag:String="Lifecycle"
    lateinit var btn_1: Button
    lateinit var btn_minus: Button
    lateinit var txt_1:TextView
    lateinit var MainViewModel:MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_1=findViewById(R.id.btn_1)
        btn_minus=findViewById(R.id.btn_minus)
        txt_1=findViewById(R.id.txt_1)


        Log.i("mainviewmodel","main activity")

        MainViewModel =ViewModelProvider(this).get(MainViewModel::class.java)


        btn_1.setOnClickListener {
          MainViewModel.add(Integer.parseInt(txt_1.text.toString()))
        }
        btn_minus.setOnClickListener {


            MainViewModel.add(Integer.parseInt(txt_1.text.toString()))
        }
        MainViewModel.score.observe(this, Observer {


            txt_1.text = it.toString()
        })
    }




}